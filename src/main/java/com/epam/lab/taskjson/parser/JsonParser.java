package com.epam.lab.taskjson.parser;

import com.epam.lab.taskjson.model.Flower;
import com.google.gson.Gson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

import static com.epam.lab.taskjson.Constants.FLOWERS_JSON_PATH;

public class JsonParser {
    private static final Logger LOGGER = LogManager.getLogger(JsonParser.class);

    public static void main(String[] args) {
        Arrays.stream(parseJson(FLOWERS_JSON_PATH)).forEach(LOGGER::info);
    }

    private static Flower[] parseJson(String pathName) {
        String json = null;
        try {
            json = new String(Files.readAllBytes(Paths.get(pathName)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Flower[] flowers = new Gson().fromJson(json, Flower[].class);
        return flowers;
    }
}
