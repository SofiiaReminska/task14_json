package com.epam.lab.taskjson;

public class Constants {
    public static final String FLOWERS_JSON_PATH = "src\\main\\resources\\json\\flowers.json";
    public static final String FLOWERS_JSON_SCHEMA_PATH = "src\\main\\resources\\json\\flowers.schema.json";
    public static final String JSON_V4_SCHEMA_IDENTIFIER = "http://json-schema.org/draft-04/schema#";
    public static final String JSON_SCHEMA_IDENTIFIER_ELEMENT = "$schema";
}
