package com.epam.lab.taskjson.validator;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;

import static com.epam.lab.taskjson.Constants.FLOWERS_JSON_PATH;
import static com.epam.lab.taskjson.Constants.FLOWERS_JSON_SCHEMA_PATH;

public class Main {
    private static final Logger LOGGER = LogManager.getLogger(Main.class);

    public static void main(String[] args) throws IOException, ProcessingException {
        File schemaFile = new File(FLOWERS_JSON_SCHEMA_PATH);
        File jsonFile = new File(FLOWERS_JSON_PATH);

        if (JsonValidator.isJsonValid(schemaFile, jsonFile)) {
            LOGGER.info("Valid!");
        } else {
            LOGGER.error("NOT valid!");
        }
    }
}
