package com.epam.lab.taskjson.model;

public enum Multiplying {
    LEAF, CUTTINGS, SEED;
}
