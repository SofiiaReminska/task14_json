package com.epam.lab.taskjson.model;

public enum Soil {
    PODZOLIC, SOIL, SOD_PODZOLIC;
}
