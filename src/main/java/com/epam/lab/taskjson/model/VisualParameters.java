package com.epam.lab.taskjson.model;

public class VisualParameters {
    private String stemColor;
    private String leafColor;
    private double averagePlantSize;

    public String getStemColor() {
        return stemColor;
    }

    public void setStemColor(String stemColor) {
        this.stemColor = stemColor;
    }

    public String getLeafColor() {
        return leafColor;
    }

    public void setLeafColor(String leafColor) {
        this.leafColor = leafColor;
    }

    public double getAveragePlantSize() {
        return averagePlantSize;
    }

    public void setAveragePlantSize(double averagePlantSize) {
        this.averagePlantSize = averagePlantSize;
    }

    @Override
    public String toString() {
        return "VisualParameters{" +
                "stemColor='" + stemColor + '\'' +
                ", leafColor='" + leafColor + '\'' +
                ", averagePlantSize=" + averagePlantSize +
                '}';
    }
}
